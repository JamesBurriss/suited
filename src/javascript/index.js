import '../sass/vendor.scss';
import '../sass/style.scss';
import axios from 'axios';

const input = document.getElementById('search-field');
const results = document.getElementById('results');
const searchContainer = document.getElementsByClassName('search-container')[0];
const welcome = document.getElementsByClassName('welcome')[0];
const technicalTraitsCarousel = document.getElementsByClassName('technical-traits-carousel')[0];
const showHide = document.getElementsByClassName('skill-overview')[0];
const searchInput = document.getElementsByClassName('spotlight-input')[0];
const personality = document.getElementsByClassName('personality')[0];
const technicalTraitsSection = document.getElementsByClassName('technical-traits-section')[0];
const technicalContainer = document.getElementsByClassName('technical-container')[0];
const personalityTraitsElement = document.getElementsByClassName('personality-trait')[0];

searchInput.value = '';

document.addEventListener('click', function(e) {
    if (e.target && e.target.className === 'result') { // if you click a result
        searchContainer.classList.add('move-up-screen');
        results.innerHTML = '';
        
        searchInput.value = e.target.innerText.trim();

        const ID = e.target.getAttribute('data-job-id');

        loadJob(ID);
    }
});

input.addEventListener('keyup', function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        // TODO: Only allow them to press enter if we have results

        results.querySelectorAll('li.result')[0].click();
    } else {
        const { value } = input;

        if (value.length < 2) {
            results.innerHTML = '';
            return;
        }

        axios.get(`http://0.0.0.0:8080/v1/job-search?searchTerm=${value}`)
            .then((response) => {
                const jobs = response.data;
                const jobsList = jobs.map(({ ID, company, logo, title }) => {
                    return `
                        <li class="result" data-job-id="${ID}">
                            ${company} <span class="subtle">${title}</span> <img src="${logo}" width="16" height="16" />
                        </li>`;
                });

                results.innerHTML = jobsList.join('');
            });
    }
});

function startPersonalityPortfolio(personalityTraits){
    const recruiterUsed = personalityTraits[0].trait.recruiterUsed;
    personalityTraitsElement.innerText = recruiterUsed;
    
    setInterval(() => {
        personality.style.display = 'inline-block';
    }, 300);
}

function startLanguageCarousel(carousel, elements) {
    carousel.style.display = 'inline-block';
    
    elements = elements.slice(0, 5);
    const carouselList = elements.map(element => {
        return `<li>${element}</li>`;
    });

    carousel.innerHTML = carouselList.join('');

    setInterval(() => {
        elements = switchCarousel(carousel, elements);
    }, 2500);
    setTimeout(() => {
        showHide.style.opacity = '1';
    }, 3000);

    setTimeout(() => {
        showHide.style.opacity = '1';
    }, 3000);
}

function switchCarousel(carousel, array) {
    array.unshift(array.pop());

    requestAnimationFrame(() => {
        carousel.style.cssText = 'display: inline-block; opacity: 1; width: 150px; font-size: 25px; height: 180px; transition: margin-top 0 ease, opacity 1s ease; margin-top: 25px;';


        Array.from(carousel.children).forEach((child, i) => {
            child.innerHTML = array[i];
        });
    
        requestAnimationFrame(() => {
            carousel.style.cssText = 'display: inline-block; opacity: 1; width: 150px; font-size: 25px; height: 180px; transition: margin-top 1.5s ease, opacity 1s ease; margin-top: 55px;';
        });
    });
    

    return array;
}

function loadJob(ID) {
    axios.get(`http://0.0.0.0:8080/v1/job-search/${ID}`)
        .then((response) => {
            const jobDescription = response.data;
            const { personalityTraits, technicalTraits, educationalTraits } = jobDescription.desired;
            const technicalTraitNames = technicalTraits.map(technicalTrait => technicalTrait.trait.recruiterUsed);
            
            setTimeout(function() {
                welcome.style.display = 'block';
            }, 250);
            
            setTimeout(function() {
                // jobRequirements.style.display = 'inline-block';
                
                //startLanguageCarousel(personalityTraitsCarousel, personalityTraits);
                startPersonalityPortfolio(personalityTraits);
                startLanguageCarousel(technicalTraitsCarousel, technicalTraitNames);
                startTechnicalPortfolio(technicalTraits);
                //startLanguageCarousel(educationalTraitsCarousel, educationalTraits);
            }, 750);     
        });
}

function startTechnicalPortfolio(technicalTraits) {
    let technicalPortfolio = '';

    technicalTraits.forEach((technicalTrait) => {
        const { trait, evidence } = technicalTrait;

        technicalPortfolio += `
            <div class="row skill">
                <div class="col specialities">
                <h2>${trait.recruiterUsed}</h2>
                <p>${trait.explanation}</p>
                <div class="frameworks">
                    ${trait.frameworks.map((framework) => {
                        return `<h3>&rdsh;${framework}</h3>`
                    }).join('')}
                </div>
                </div>
                <div class="col">
                    <div class="row">
                        ${evidence.map((item) => {
                            return `
                                <div class="col-6">
                                    <img src="${item.thumbnail}">
                                </div>
                            `;
                        }).join('')}
                    </div>
                </div>
            </div>
        `;
    });

    technicalTraitsSection.innerHTML = technicalPortfolio;
    technicalContainer.style.display = 'initial';
}
